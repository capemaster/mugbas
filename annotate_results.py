# This script is intended to be launched by the bash wrapper mugba.sh
# as part of the multispecies gene based association suite MUGBAS
# It starts loading the Annotation, the mapped file list and searches
# for matches between the two lists.

annotation = 'annotation_nodup.txt' 					# Annotation
usermap = open('mapped_genes.list', 'r')				# Mapped genes
annotated = open('results/annotated_genes.txt', 'w')	# Output file

lines={}
for a in usermap:
	a=a.strip()
	lines[a]=a

# lines = usermap.read()
i = 0
count = 0

# Prepare the header of the annotated_genes.txt file
annotated.write('Chr\tStart\tEnd\tFeatureID\tCustomField1\tCustomField2\tCustomField3\n')

# Start iterating and searching for matches
with open(annotation, 'r') as f:
	for line in f:
		gene = line.split("\t")[3]
		if lines.has_key(gene):
			annotated.write(line)
		i = i+1
		if i%5000 == 0:
			count = count + 5000
			print "Lines Processed: " + str(count)
print "Processed Lines: " + str(i)
