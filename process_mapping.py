# This python program is part of the MUGBA suite
# Its purpose is to process the bedtools mapping output and creating a file for each feature.
# In the file are stored the names of the markers that falls in the feature boundaries.

import sys, os

inf=sys.argv[1]												# file with SNPS in genes
numsnp=sys.argv[2]											# threshold

for ena,a in enumerate(open(inf)):							# open IN_file
	fields=a.strip().split("\t")							# separate the fields
	
	if ena==0:												# if it' the forst line define gene
		genename=fields[3]
		outf=open("mapping/"+genename,"w")					# open output file
		count=0												# count number of SNP
		
	if (fields[3]==genename):								# if the feature is the same in the current and in the previous line
		outf.write(fields[7]+"\n")							# prints the SNP name in the feature file
		count=count+1										
		
	else:													# no more SNP in the feature, closing connection
		outf.close()
		
		if (int(count)<int(numsnp)):									# remove file if has less than n SNPS
			os.remove("mapping/"+genename)		
		
		genename=fields[3]
		outf=open("mapping/"+genename,"w")					#Open the new feature file
		outf.write(fields[7]+"\n")
		count=1

# final step
outf.close()
if (int(count)<int(numsnp)):
	os.remove("mapping/"+genename)				
