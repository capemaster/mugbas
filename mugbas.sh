#!/bin/bash

##########################################################################################
#                    __  __ _    _  _____ ____           _____                           #
#                   |  \/  | |  | |/ ____|  _ \   /\    / ____|                          #
#                   | \  / | |  | | |  __| |_) | /  \  | (___                            #
#                   | |\/| | |  | | | |_ |  _ < / /\ \  \___ \                           #
#                   | |  | | |__| | |__| | |_) / ____ \ ____) |                          #
#                   |_|  |_|\____/ \_____|____/_/    \_\_____/                           #
#                                                                                        #
#                      MUltispecies Gene Based Association Suite                         #
#                                                                                        #
##########################################################################################
#                           FUNCTIONS AND DEPENDENCIES CHECK                             #
##########################################################################################

# Error checking by BASH
set -u
set -e

# Small function to provide better log messaging at screen
log() {
    msg=$1
    date=`date`
    printf "$date - $msg\n" | tee -a mugbas.log
}


# Checking if program dependencies are satisfied
command -v bedtools >/dev/null 2>&1 || { echo >&2 "MUGBAS requires bedtools but is not but in your path. Aborting."; exit 1; }
command -v python >/dev/null 2>&1 || { echo >&2 "MUGBAS requires python but is not but in your path. Aborting."; exit 1; }
command -v parallel >/dev/null 2>&1 || { echo >&2 "MUGBAS requires GNU parallel but is not but in your path. Aborting."; exit 1; }
command -v R >/dev/null 2>&1 || { echo >&2 "MUGBAS requires R but is not but in your path. Aborting."; exit 1; }

# R packages dependencies check
R --vanilla --slave <<RSCRIPT
write.table(.packages(all.available=T),'packages.txt',quote=F,col.names=F,row.names=F)
RSCRIPT
grep -w "chopsticks" packages.txt > /dev/null 2>&1 || { echo >&2 "MUGBAS requires chopsticks package. Aborting."; exit 1; }
grep -w "GenABEL" packages.txt > /dev/null 2>&1 || { echo >&2 "MUGBAS requires GenABEL package. Aborting."; exit 1; }
grep -w "doParallel" packages.txt > /dev/null 2>&1 || { echo >&2 "MUGBAS requires doParallel package. Aborting."; exit 1; }
grep -w "foreach" packages.txt > /dev/null 2>&1 || { echo >&2 "MUGBAS requires foreach package. Aborting."; exit 1; }
grep -w "R.utils" packages.txt > /dev/null 2>&1 || { echo >&2 "MUGBAS requires R.utils package. Aborting."; exit 1; }
grep -w "corpcor" packages.txt > /dev/null 2>&1 || { echo >&2 "MUGBAS requires corpcor package. Aborting."; exit 1; }
grep -w "mvtnorm" packages.txt > /dev/null 2>&1 || { echo >&2 "MUGBAS requires mvtnorm package. Aborting."; rm packages.txt && exit 1; }

rm packages.txt

##########################################################################################
#                                     WELCOME MESSAGE                                    #
##########################################################################################

printf "Welcome to \n"
printf "     __  __ _    _  _____ ____           _____  \n"
printf "    |  \/  | |  | |/ ____|  _ \   /\    / ____| \n"
printf "    | \  / | |  | | |  __| |_) | /  \  | (___   \n"
printf "    | |\/| | |  | | | |_ |  _ < / /\ \  \___ \  \n"
printf "    | |  | | |__| | |__| | |_) / ____ \ ____) | \n"
printf "    |_|  |_|\____/ \_____|____/_/    \_\_____/  \n\n"
printf "     MUltispecies Gene Based Association Suite  \n\n"
printf "ver 1.0 - last revision: 09-01-2015          \n\n\n"

##########################################################################################
#                        READ THE PARAMETERS AND SETTING DEFAULTS                        #
##########################################################################################

defaultinpedfile="example"
defaultgwasresultsdir="example_traits/"
defaultnumcore="2"
defaultgwfdr="0.05"
defaultannotation="example_annotation.txt"
defaultbuffer="100000"
defaultsnpmin="2"

read -e -p "#    LD    # Name of the .ped file (NO EXTENSION) [default value: $defaultinpedfile]: " inpedfile
read -e -p "# GENEWISE # Input the folder with the GWAS results (see README) [default value: $defaultgwasresultsdir]: " gwasresultsdir
read -p "# GENEWISE # Enter the maximum number of cores you want use [default value: $defaultnumcore]: " numcore
read -p "# GENEWISE # Enter the threshold value for the GenWise significance (FDR) [default value: $defaultgwfdr]: " gwfdr
read -e -p "# MAPPING  #  Annotation file (BED format) [default value: $defaultannotation]: " annotation
read -p "# MAPPING  #  Buffer zone for mapping SNP to features (in bp) [default value: $defaultbuffer]: " buffer
read -p "# MAPPING  #  Minimum number of SNPs to consider in a feature [default value: $defaultsnpmin]: " snpmin

inpedfile=${inpedfile:-$defaultinpedfile}
gwasresultsdir=${gwasresultsdir:-$defaultgwasresultsdir}
numcore=${numcore:-$defaultnumcore}
gwfdr=${gwfdr:-$defaultgwfdr}
annotation=${annotation:-$defaultannotation}
buffer=${buffer:-$defaultbuffer}
snpmin=${snpmin:-$defaultsnpmin}

# Logging user defined parameters
echo "PED file: $inpedfile" >> mugbas.log
echo "Trait folder: $gwasresultsdir" >> mugbas.log
echo "Cores: $numcore" >> mugbas.log
echo "FDR: $gwfdr" >> mugbas.log
echo "Annotation File: $annotation" >> mugbas.log
echo "Buffer zone: $buffer" >> mugbas.log
echo "Min SNP: $snpmin" >> mugbas.log

# Checking if input files and directories exist

# PED File
if [ ! -f "$inpedfile.ped" ]
then
    printf "There is not any $inpedfile.ped file in the working path!\n"
    printf "Check the spelling or copy it the mugbas directory\n"
    exit 1;
fi

# MAP file
if [ ! -f "$inpedfile.map" ]
then
    printf "There is not any $inpedfile.map file in the working path!\n"
    printf "Check the spelling or copy it the mugbas directory\n"
    exit 1;
fi

# Traits Directory
if [ ! -d "$gwasresultsdir" ]
then
    printf "There is not any $gwasresultsdir directory in the working path!\n"
    printf "Check the spelling or copy it the mugbas directory\n"
    exit 1;
fi

# Check if header in trait datafiles is correct
for traitfile in `ls $gwasresultsdir`
do
    head -1 $gwasresultsdir/$traitfile | grep -w SNPNAME > /dev/null 2>&1 || { echo >&2 "Check your trait datafiles. There is no SNPNAME column in $traitfile"; exit 1; }
    head -1 $gwasresultsdir/$traitfile | grep -w PVALUE > /dev/null 2>&1 || { echo >&2 "Check your trait datafiles. There is no PVALUE column in $traitfile"; exit 1; }
done

# Annotation File
if [ ! -f "$annotation" ]
then
    printf "There is not any $annotation file in the working path!\n"
    printf "Check the spelling or copy it the mugbas directory\n"
    exit 1;
fi

# Checking if ped files has at least 200 lines
lines=$(wc -l "$inpedfile".ped | awk '{print $1}')
if [ $lines -lt 200 ]; then
    printf "#################################################\n"
    printf "#                   \033[31mWARNING\e[0m                     #\n"
    printf "#       You have less than 200 individuals!     #\n"
    printf "#       LD calculation maybe not reliable!      #\n"
    printf "#################################################\n"
fi


##########################################################################################
#                                      MAPPING STEP                                      #
##########################################################################################

# Starting the analysis
printf "\nAll parameters are set.. Starting the analysis\n\n"
today=`date +%Y%m%d%H%M` # Gets the actual time Year month day Hour Minute

# Starts the analysis if the mapping directory does not exists
# Ask for remapping with the last inserted parameters and rename old mapping. # implement with date or boundaries.

if ! [ -d mapping ]
then

    mkdir mapping
    # checking annotation, purging from duplicate entries and sorting #### ragioniamo qui
    cat $annotation | sort -k4,4 -u > annotation_nodup.txt

    # Prepares the annotation file in the correct format (just a double check)
    cat annotation_nodup.txt | sort -k1,1 | awk '{ print $1"\t"$2"\t"$3"\t"$4 }' > annotation.txt
    
    # Prepare the snps.bed file starting from the map file
    awk '{ print $1"\t"$4-1"\t"$4"\t"$2}' $inpedfile.map > snps.bed
    
    log "Mapping SNPS to features and removing those with less than $snpmin SNPs inside"
    cat annotation.txt | bedtools window -a - -b snps.bed -w $buffer > snps_on_genes.bed
    python process_mapping.py snps_on_genes.bed $snpmin
    
    # Clean up the service files
    rm annotation.txt

    log "Done mapping and filtering."
    
else
    
    defaultremap="no"
    log "\033[31m#### It seems that you already mapped your SNPs ###\e[0m"
    read -p "Do you want proceed with a new mapping process? [yes/no] " remap
    
    remap=${remap:-$defaultremap}
    
    if [ $remap = "yes" ];
    then
        mv mapping/ mapping-"$today"/
        log "\033[33mRenamed old mapping directory as mapping-$today\e[0m"
        
        # Check the ldmatrices directory presence, and move the old one in the renamed version
        if [ -d ldmatrices ]
            then
            mv ldmatrices/ ldmatrices-"$today"/
            log "\033[33mRenamed old ldmatrices directory as ldmatrices-$today\e[0m"
        fi
        
        mkdir mapping
        cat $annotation | sort -k4,4 -u > annotation_nodup.txt
        cat annotation_nodup.txt | sort -k1,1 | awk '{ print $1"\t"$2"\t"$3"\t"$4 }' > annotation.txt
        awk '{ print $1"\t"$4-1"\t"$4"\t"$2}' $inpedfile.map > snps.bed
        log "Mapping SNPS to features and removing those with less than $snpmin SNPs inside"
        cat annotation.txt | bedtools window -a - -b snps.bed -w $buffer > snps_on_genes.bed
        python process_mapping.py snps_on_genes.bed $snpmin
        rm annotation.txt
        log "Done mapping and filtering."
    else
        log "Using old mapping..."
    fi
fi

##########################################################################################
#                                     LD CALCULATION                                     #
##########################################################################################

log "Preparing GenABEL input files for LD calculation"

# Select individuals for LD calculation
if [ $lines -lt 200 ]; then
    # If ped file has less than 200 individuals all are retained for LD calculation
    cp $inpedfile.ped "$inpedfile"_LDsubset.ped
else
    # Select 200 random individuals for LD calculation
    cat $inpedfile.ped | python -c "import random, sys; random.seed(100); print ''.join(random.sample(sys.stdin.readlines(), 200))," > "$inpedfile"_LDsubset.ped
fi

# Creating adjusted map for GenABEL input
awk '{print $1"\t"$2"\t"$4}' $inpedfile.map > correctedmap.map

# Preparing fake phenotype file
printf "id\tsex\tfake\n" > phenotype.txt
awk '{print $2"\t1\t-9"}' "$inpedfile"_LDsubset.ped >> phenotype.txt

# LD calculation process
if  ! [ -d ldmatrices ]
then
    log "Calculating LD... Please wait!"
    mkdir ldmatrices
    
    # Calculation of LD
    R < LDmatrices_creation.R --slave --args $inpedfile $numcore > ld.log
    
else
    log "\033[31m#### It seems that you already calculated LD ###\e[0m"
    log "Using old LD calculation..."
fi

# implement check in length ls of the two directories

##########################################################################################
#                              GENE-WISE PVALUE CALCULATION                              #
##########################################################################################

if [ -d results ]
then
    log "\033[33mRenamed old results/ directory as results-$today\e[0m"
    mv results/ results-"$today"/
fi

mkdir results

log "Calculating GeneWise pvalue for each feature, in each phenotype"

let "ntraits=`ls $gwasresultsdir | wc -l`"
div=$((numcore / ntraits))

if [ $div = 0 ]
then
    ncoreP=$numcore
    ncoreR=1
else
    ncoreP=$ntraits
    ncoreR=$div
fi

# Parallelize the GeneWise calculation for each trait
ls $gwasresultsdir | parallel --no-notice -j"$ncoreP"  R '<' genewise_calculation.R --slave --args {} $inpedfile $gwasresultsdir $ncoreR '>' {.}.genewise.log

##########################################################################################
#                                    RESULTS ANNOTATION                                   #
##########################################################################################

# Checking that all necessary files are there
if ! [ -f annotation_nodup.txt ]
then
    # checking annotation and purging from duplicate entries
    cat $annotation | sort -k4,4 -u > annotation_nodup.txt
    log "Annotating your results"
else
    log "Annotating your results"
fi

# retrieving the feature list
ls ldmatrices > mapped_genes.list

# launching python to filter for the analyzed feature
python annotate_results.py > gene_annotation.log

cd results
mkdir logs
ls 2* | parallel --no-notice 'paste {} annotated_genes.txt  > Temp_{.}.txt'
cd ..

##########################################################################################
#                              RESULTS: PLOT and INTEGRATING                             #
##########################################################################################

log "Plotting GeneWise p-value and FDR results, for each phenotype"

ls "results/Temp_"* | parallel -j"$numcore"  R '<' plot_results.R --slave --args {}  '>' plot.log

### Only Significant results
log "Create files with only significant features, for each phenotypes"

cd results
for a in `ls Annotated_*`
do 
newfile="Significant_"$a
head -1 $a > $newfile
awk -v soglia="$gwfdr" '$5<=soglia {print}' $a >> $newfile
done
cd ..

##########################################################################################
#                           CLEAN UP THE MESS IN THE DIRECTORY                           #
##########################################################################################

log "Cleaning the directories..."

# Move files to results directory
rm -rf *_LDsubset* annotation_nodup.txt results/annotated_genes.txt mapped_genes.list snps_on_genes.bed snps.bed results/Temp_* results/2*

# End of analysis
log "Analysis finished! Check out the \033[31mresults/\e[0m directory\n"
mv *.log results/logs