#What is MUGBAS?#

MUGBAS is a open source pipeline for obtaining gene based results from a given GWA study. It is composed by different scripts written in Bash, R and Python. The software is an adaptation, with some modifications, of [VEGAS](http://gump.qimr.edu.au/VEGAS/) by Liu and colleagues [[doi](http://dx.doi.org/10.1016/j.ajhg.2010.06.009)], recoded in order to work with no species nor feature constraints.

---
## Overview

MUGBAS (MUltispecies Gene Based Association Suite) consists of several programs written in different programming languages that are supposed to be launched from `mugbas` wrapper:

> `mugbas.sh`	- 	Bash wrapper

> `process_mapping.py`	-	Script that process the mapped SNPS and genes

> `LDmatrices_creation.R` 	-	LD calculation with GenABEL library

> `genewise_calculation.R`	-	Calculates the GeneWise p-value in R

> `annotate_results.py`	-	Program that annotates the results

> `plot_results.R`	-	Manhattan and underground plot creation


## Usage
### Installation
Installing MUGBAS is an easy task: simply [download](https://bitbucket.org/capemaster/mugbas/downloads) the repository in zip format  or clone the repository in any location in your machine. The command to clone the repository in your system is the following:
```
$ git clone --recursive https://bitbucket.org/capemaster/mugbas.git
```
Once the `mugbas/` directory is available, the program should be ready to run, if dependencies are satisfied.

### Dependencies
MUGBAS relies on several dependencies that can be easily fulfilled:

- [R](http://cran.r-project.org/) 3.0.2 or newer and the following packages
	- chopsticks
	- GenABEL
	- doParallel
	- foreach
	- R.utils
	- corpcor
	- mvtnorm
- [bedtools](http://code.google.com/p/bedtools/downloads/list), 2.6.2 or newer
- [GNU parallel](http://www.gnu.org/software/parallel/) ver20140122 or newer
- [perl](http://www.perl.org) as required by parallel version or newer
- [python](http://www.python.org) 2.7 

Programs like _GNU parallel_, _bedtools_ as well as _python_ and _perl_ languages are required to be in your `PATH` environment variable. If you don't know how to do this, please refer to [this page](http://unix.stackexchange.com/questions/26047/how-to-correctly-add-a-path-to-path). Please note that the correct `PATH` setting procedure is shell dependent. To check which shell environment you are running type this command:

```$ ps -p $$```
or 

```$ echo $0```

A simple way to install all R dependencies in a bundle fashion is provided below. Just log into your R environment and paste these few lines.

```
source("http://bioconductor.org/biocLite.R")
biocLite("chopsticks")

install.packages(c("GenABEL","R.utils","corpcor","mvtnorm","foreach","doParallel"))

```
This procedure should install automatically all the R packages.

For _GNU parallel_ and _bedtools_ installation is just a `configure` and `make` procedure, but details are provided with the programs themselves.
Lastest releases can be found [here](http://bedtools.readthedocs.org/en/latest/content/installation.html) for _bedtools_, and [here](http://ftp.gnu.org/gnu/parallel/parallel-latest.tar.bz2) for _GNU parallel_.
Please note that some dependencies can be satisfied also using dedicated package managers. For example [MacPorts](http://www.macports.org/) on Mac OS X, [yum](http://yum.baseurl.org/) or on several Linux installations.
Mac users **must** have [Xcode](https://developer.apple.com/xcode/downloads/) installed on their machine along with the Command Line Utilities to install MacPorts. These tools are provided for free via the AppStore.

However, there could be discrepancies between the availability of the lastest version from the developers and the one provided with the packet manager (usually older).

Please note that MUGBAS also depends on other bash utilities that are commonly installed on Linux/Unix/Mac systems. You should not have problems after dependencies satisfaction, but please report any problem or bug at [this page](https://bitbucket.org/capemaster/mugbas/issues/new).

### Input preparation

MUGBAS input files are the following:

- a PLINK .ped and .map data in ACTG format of your subjects. Details on this format are provided [here](http://pngu.mgh.harvard.edu/~purcell/plink/). MUGBAS requires **at least 200** individuals to correctly estimate linkage disequilibrium between markers. If you don't have this requirement, MUGBAS will perform the analysis warning you about results reliability;
- a text file with two columns flagged as SNPNAME (where there are SNPs names) and PVALUE, where are listed the corresponding p-values obtained with the GWAS analysis;
- an annotation file, **tab separated with no header**, as the following example:

```
27	24193122	24209942	ENSBTAG00000009685	THEX1	ERI1	exoribonuclease 1
20	13704346	13749692	ENSBTAG00000016903	NLN	NLN	neurolysin (metallopeptidase M3 family)
20	13704346	13749692	ENSBTAG00000016903	NLN	LOC100847883	neurolysin, mitochondrial-like
```

Where columns are the following: chr, start, end, EnsemblGeneID, AssociatedGeneName, WikiName, WikiDescription. 
This type of file can be easily created using the biological query platform [BioMart](http://www.ensembl.org/biomart).

Please note that the example annotation is gene-centric but MUGBAS can work with any type of genomic coordinates. However, users have to respect the following rules:

- chr, start, end and FeatureID **must** be provided in this order.
- the other three columns are customizable but **must be provided as well**: if you don't have any data add `NA` in the empty field.

### Running the suite

Running MUGBAS is super-easy: just open a shell and **point where your data files and scripts are living together**.
Type `./mugbas.sh` and tune the analysis answering to the questions at screen. You will be prompted to insert input file, traits path, mapping boundaries, CPUs and FDR threshold.
Analysis should start if all parameters are correctly inserted.
If you receive this error:
```
$ bash: ./mugbas.sh: Permission denied
```
Try 
```
$ chmod +x mugbas.sh
``` 
to make the file executable and re-run the command.

Please remember that all the programs and the input data **must** live within the same directory.
A ready to go example is available in the `trialdata/` directory of this repository.

MUGBAS has been successfully tested on a variety of systems:

- Mac OSX 10.7.5 and above
- RHEL 6.4/6.5
- CentOS 6.5
- Ubuntu 12.04



### Performance

Depending on your system, marker density, and on the level of parallelization you choose, a complete MUGBAS analysis (LD estimation + GeneWise calculation) can range from minutes to hours.
MUGBAS performance on a distributed analysis (10 CPUs, Intel Xeon X5675 @ 3.07 GHz) are showed below.

|          |  Low Density (50K Markers)  |  High Density (800K markers) |
|----------|:---------------------------:|:----------------------------:|
| 1 Trait  | 8 minutes / 2.5 Gb RAM Peak | 100 minutes / 8 Gb RAM Peak  |
| 3 Traits | 12 minutes / 2 Gb RAM Peak  | 280 minutes / 16 Gb RAM Peak |

LD estimation ranges from 4 minutes to 20 minutes with 10 cores.


### Checking the results
If the analysis ends correctly, all your results should be contained in the `results/` directory, along with plots and log files.

This directory contains two text files per trait, one with the full results and one only  with the features that overcome the chosen FDR threshold (file tagged as "Significant"). Two plots in .png format for each analyzed trait: Manhattan plot of the obtained p-values in -log10 scale for each chromosome, and an Underground plot with the corresponding FDR value. In the `logs/` directory are stored all the log files from the analysis. File naming is self-explanatory.

### Windows users
Windows users cannot use MUGBAS out of the box. There could be a "[Cygwin](https://www.cygwin.com) way" to use MUGBAS on your Windows based system, but we can't help with the set up as well as the troubleshooting. The simplest way could be install Linux Virtual Machine using the free utility [VirtualBox](http://www.virtualbox.org).

## Citation
If you use MUGBAS, please cite our application note published in Bioinformatics along with the other tools used in the suite.

**MUGBAS: a species free gene-based programme suite for post-GWAS analysis**
S. Capomaccio; M. Milanesi; L. Bomba; E. Vajana; P. Ajmone Marsan
Bioinformatics first published online March 11, 2015
doi: [10.1093/bioinformatics/btv144](http://dx.doi.org/10.1093/bioinformatics/btv144)


## Disclaimer

MUGBAS is a free tool that uses free software that is publicly available online: you can redistribute this pipeline and/or modify this program, but at your own risk. MUGBAS is distributed with the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details: http://www.gnu.org/licenses/.
This pipeline is for research and has not a commercial intent, but it can be used freely by any organization. The authors of this pipeline are not responsible for ANY output, modification or result obtained from it. For bug report, feedback and questions (PLEASE read the carefully this README file before sending your question) please point your browser to [this page](https://bitbucket.org/capemaster/mugbas/issues/new).